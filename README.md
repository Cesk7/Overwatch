# **Une nouvelle map bientôt jouable ?**

**Jouez à Overwatch commence à vous lassez ? Blizzard est la pour vous remotiver.**


Dans le cadre de l'événement pour l'anniversaire d'Overwatch, une nouvelle carte de match à mort (Deathmatch pour les intimes), Petra, a été lancée avec comme thème l'archéologie. La nouvelle carte est dotée d'un sol cassable qui tombe pour révéler la "fosse de malheur" une première dans Overwatch, qui devrait en ravire plus d'un.

_Cliquez pour lire la vidéo ;)_
[![Everything Is AWESOME](https://i.imgur.com/mfevmeH.jpg)](https://www.youtube.com/watch?v=YSvm-_NxZXA "Everything Is AWESOME")
**Note:** _A noté qu'à aucuns moments ils y font allusion dans la vidéo._

<p> Il y a encore beaucoup à apprendre sur Petra, par exemple la carte a été inspirée de la vrai cité de Petra située en actuelle Jordanie et qui a tout de même été construite au VIIIe siècle av. J.-C, voilà de quoi vous aidez à situer la chose: </p>

![petra](/uploads/933ce37e257f9b7e0489b213b1ae32d3/petra.PNG)
**Note:** _Il y a une petite ressemblance vous ne trouvez pas ? :)_


<p> Après ce rapide cour d'Histoire, nous allons nous intéressé à la vidéo que Blizzard nous a fournis, une vidéo qui est pleine de mystères (vous ne les avez pas vus ? :p) les voilà: </p>

![processtttt](/uploads/40e68c2a86a71c1f34b051f782ed69e1/processtttt.png)

<p> Le premier message mentionne Ilios(les cartes en Grèce), et un nouveau projet en Thaïlande. Il présente aussi quelqu'un appelé Dr. Nachareon et a été écrit par H. Faisal, vraisemblablement Hamid Faisal à en juger par le nom de connexion sur la photo suivante.</p>

![hamid](/uploads/d85859ed3b72f144617d9f22b8e01191/hamid.png)

<p> Nous pouvons aperçevoir en premier plan un ordinateur qui est sur une page d'authentification. En plein milieu de l'écran il y a cette nouvelle icône mystèrieuse qui fait sont apparition pour la première fois dans Overwatch, et juste en-dessous le supposé prénom du premier protagoniste. La question qu'on peut maintenant se poser est quelle organisation finance cette expédition archéologique et que cherchent-ils? Et à quoi ressemblent-ils ?</p>

![process33](/uploads/4172740acde2e5d76ef336bf0904d045/process33.png)

<p> Sur ce troisième screenshot, nous pouvons voir de nouveau un Pc (la page qui suit l'écran d'authentification ?) mais cette fois-ci on aperçoit très clairement une discussion entre deux personnes, la première appelée Iaonnidis, et le deuxième qui est Faisal, notre premier protagoniste, qui décrit "une chambre cachée accessible uniquement par une ouverture étroite". La chambre cachée pourrait-elle être quelque part sur la carte de Petra ?</p>

<p> Overwatch est connu pour envoyer ses joueurs au casse-pîpe quand il s'agit de chercher des indices qui révéleraient de nouveaux héros ou de nouvelles cartes. Ces messages trouvés dans Petra pourraient révéler plus que ce que les les fans attendent.</p>
<p> Alors nouveaux personnages ? Nouvelles cartes en Thaïlande peut-être ? Espèrons que nous pourrons en apprendre plus sur l'identité et sur les intentions de ses personnages. </p>